/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kuma.wechat4j.server;

import com.alibaba.fastjson.JSONObject;
import com.kuma.wechat4j.constant.WeChatConstant;
import com.kuma.wechat4j.model.TokenModel;
import com.kuma.wechat4j.utils.HttpUtils;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * AccessToken请求类
 *
 * @author Kuma
 */
public class TokenServer {

    private TokenModel accessToken;

    public boolean request(TokenModel atm) {
        Map<String, Object> params = new HashMap<>();
        params.put("grant_type", atm.getGrant_type());
        params.put("appid", atm.getAppid());
        params.put("secret", atm.getSecret());

        JSONObject result = HttpUtils.getJson(WeChatConstant.TOKEN, params);
        accessToken = result.toJavaObject(TokenModel.class);
        if (accessToken == null) {
            return false;
        }

        if (checkData()) {
            accessToken.setTokenTime(new Date().getTime());
            return true;
        } else {
            return false;
        }
    }

    private boolean checkData() {
        if (accessToken == null) {
            return false;
        }
        return !(accessToken.getAccess_token() == null || accessToken.getExpires_in() == null);
    }

    /**
     *
     * @return TokenServer 对象
     */
    public TokenModel getAccessToken() {
        return this.accessToken;
    }

    /**
     *
     * @return AccessToken字符串
     */
    public String getToken() {
        return this.accessToken.getAccess_token();
    }

    /**
     *
     * @return 得到有效时间
     */
    public long getExpires() {
        return this.accessToken.getExpires_in();
    }

    /**
     * TokenServer 是否有效
     *
     * @return true:有效，false: 无效
     */
    public boolean isValid() {
        if (accessToken == null) {
            return false;
        }

        String token = accessToken.getAccess_token();
        Long expires_in = accessToken.getExpires_in();

        if (null == token || token.length() <= 0) {
            return false;
        }
        if (null == expires_in || expires_in < 1) {
            return false;
        }
        return !isExpire();
    }

    /**
     * 是否过期
     *
     * @return true 过期 false：有效
     */
    private boolean isExpire() {
        Date currentDate = new Date();
        long currentTime = currentDate.getTime();
        long expiresTime = accessToken.getExpires_in() * 1000 - accessToken.getRedundance();
        //判断是否过期
        return (accessToken.getTokenTime() + expiresTime) < currentTime;
    }
}
