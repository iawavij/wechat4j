/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kuma.wechat4j.server;

import com.kuma.wechat4j.common.Config;
import com.kuma.wechat4j.model.TokenModel;
import org.apache.log4j.Logger;

/**
 *
 * @author Kuma
 */
public enum CenterServer {
    CS;

    private final Logger LOG = Logger.getLogger(CenterServer.class);

    private String appid;
    private String secret;
    private int requestTimes = 1;   //请求失败后重新请求的次数

    private final TokenServer tokenServer;

    private CenterServer() {
        LOG.debug("微信中间服务器已经启动");
        tokenServer = new TokenServer();
        this.appid = Config.instance().getAppid();
        this.secret = Config.instance().getAppSecret();
    }

    public int getRequestTimes() {
        return requestTimes;
    }

    public void setRequestTimes(int requestTimes) {
        this.requestTimes = requestTimes;
    }

    /**
     * 通过中控服务器得到AccessToken实例
     *
     * @return
     */
    private TokenServer tokenServer() {
        //没有可用的token，则去刷新
        if (!this.tokenServer.isValid()) {
            refresh();
        }
        return this.tokenServer;
    }

    /**
     * 通过中控服务器得到accessToken字符串
     *
     * @return
     */
    public String accessToken() {
        return tokenServer().getToken();
    }

    /**
     * 服务器刷新token
     */
    public void refresh() {
        TokenModel atm = new TokenModel();
        atm.setAppid(this.appid);
        atm.setSecret(this.secret);
        for (int i = 0; i < requestTimes; i++) {
            //请求成功则退出
            if (this.tokenServer.request(atm)) {
                break;
            }
        }
    }
}
