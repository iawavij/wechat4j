/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kuma.wechat4j;

import com.kuma.wechat4j.common.Config;
import com.kuma.wechat4j.common.EventType;
import com.kuma.wechat4j.common.MsgManage;
import com.kuma.wechat4j.common.ValidateSignature;
import com.kuma.wechat4j.common.ReqType;
import com.kuma.wechat4j.model.message.req.EventRequestMessage;
import com.kuma.wechat4j.model.message.req.ImageRequestMessage;
import com.kuma.wechat4j.model.message.req.LinkRequestMessage;
import com.kuma.wechat4j.model.message.req.LocationRequestMessage;
import com.kuma.wechat4j.model.message.req.RequestMessage;
import com.kuma.wechat4j.model.message.req.TextRequestMessage;
import com.kuma.wechat4j.model.message.req.VideoRequestMessage;
import com.kuma.wechat4j.model.message.req.VoiceRequestMessage;
import com.kuma.wechat4j.model.message.resp.BaseResponseMessage;
import com.kuma.wechat4j.utils.JaxbUtils;
import com.kuma.wechat4j.utils.StreamUtils;
import javax.servlet.http.HttpServletRequest;
import org.apache.log4j.Logger;

/**
 * 抽象方法中，on开头的是msgtype的事件，无on的是event事件
 *
 * @author Kuma
 */
public abstract class WeChatSupport {

    private static final Logger LOG = Logger.getLogger(WeChatSupport.class);

    public String execute(HttpServletRequest request) {
        String signature = request.getParameter("signature");
        String timestamp = request.getParameter("timestamp");
        String nonce = request.getParameter("nonce");
        String echostr = request.getParameter("echostr");

        String token = Config.instance().getToken();

        ValidateSignature validateSignature = new ValidateSignature(signature, timestamp, nonce, token);
        if ((signature == null || timestamp == null || nonce == null) || !validateSignature.check()) {
            return "error";
        }
        if (echostr != null && echostr.length() > 0) {
            return echostr;
        }
        return dispatch(request);

    }

    private String dispatch(HttpServletRequest request) {
        String xmlStr = StreamUtils.getXmlString(request);

        long start = System.currentTimeMillis();
        BaseResponseMessage responseMessage = dispatchMessage(xmlStr);
        long end = System.currentTimeMillis();

        if (end - start > 4500) { //超过4秒半的处理时间就发送客服消息
            if (responseMessage != null) {
                sendMessage(responseMessage);
            }
            return "success"; //返回空值阻止微信重复请求
        }

        return responseMessage == null ? "success" : JaxbUtils.toXml(responseMessage);
    }

    /**
     * 发送客服消息
     *
     * @param responseMessage
     */
    private void sendMessage(final BaseResponseMessage responseMessage) {
        LOG.info("超时发送客服消息");
        new Thread(new Runnable() {
            @Override
            public void run() {
                new MessageHandle(responseMessage).send();
            }
        }).start();
    }

    /**
     * 消息处理
     */
    private BaseResponseMessage dispatchMessage(String xmlStr) {
        RequestMessage requestMessage = JaxbUtils.toBean(xmlStr, RequestMessage.class);
        ReqType msgType = ReqType.valueOf(requestMessage.getMsgType());

        // 排重
        if (MsgManage.isExist(requestMessage)) {
            LOG.warn("微信重复请求的消息，抛弃不处理，消息类型：" + msgType);
            return null;
        }

        LOG.debug("消息类型：" + msgType);
        BaseResponseMessage responseMessage = null;
        switch (msgType) {
            case event:
                responseMessage = onEvent(JaxbUtils.toBean(xmlStr, EventRequestMessage.class));
                break;
            case text:
                responseMessage = onText(JaxbUtils.toBean(xmlStr, TextRequestMessage.class));
                break;
            case image:
                responseMessage = onImage(JaxbUtils.toBean(xmlStr, ImageRequestMessage.class));
                break;
            case voice:
                responseMessage = onVoice(JaxbUtils.toBean(xmlStr, VoiceRequestMessage.class));
                break;
            case video:
            case shortvideo:
                responseMessage = onVideo(JaxbUtils.toBean(xmlStr, VideoRequestMessage.class));
                break;
            case location:
                responseMessage = onLocation(JaxbUtils.toBean(xmlStr, LocationRequestMessage.class));
                break;
            case link:
                responseMessage = onLink(JaxbUtils.toBean(xmlStr, LinkRequestMessage.class));
                break;
            default:
                LOG.warn("未知的消息类型：" + msgType);
                break;
        }

        MsgManage.clear(requestMessage); //处理完消息清除
        return responseMessage;
    }

    /**
     * 事件消息处理
     */
    private BaseResponseMessage onEvent(EventRequestMessage Message) {
        LOG.debug("事件类型：" + Message.getEvent());
        EventType eventType = EventType.valueOf(Message.getEvent());

        BaseResponseMessage responseMessage = null;
        switch (eventType) {
            case CLICK:
                responseMessage = CLICK(Message);
                break;
            case VIEW:
                responseMessage = VIEW(Message);
                break;
            case subscribe:
                responseMessage = subscribe(Message);
                break;
            case unsubscribe:
                responseMessage = unsubscribe(Message);
                break;
            case SCAN:
                responseMessage = SCAN(Message);
                break;
            case LOCATION:
                responseMessage = LOCATION(Message);
                break;
            case scancode_push:
                responseMessage = scancode_push(Message);
                break;
            case scancode_waitmsg:
                responseMessage = scancode_waitmsg(Message);
                break;
            case pic_sysphoto:
                responseMessage = pic_sysphoto(Message);
                break;
            case pic_photo_or_album:
                responseMessage = pic_photo_or_album(Message);
                break;
            case pic_weixin:
                responseMessage = pic_weixin(Message);
                break;
            case location_select:
                responseMessage = location_select(Message);
                break;
            default:
                LOG.warn("未知的消息类型：" + eventType);
                break;
        }

        return responseMessage;
    }

    protected abstract BaseResponseMessage onText(TextRequestMessage message);

    protected abstract BaseResponseMessage onImage(ImageRequestMessage message);

    protected abstract BaseResponseMessage onVoice(VoiceRequestMessage message);

    protected abstract BaseResponseMessage onVideo(VideoRequestMessage message);

    protected abstract BaseResponseMessage onLocation(LocationRequestMessage message);

    protected abstract BaseResponseMessage onLink(LinkRequestMessage message);

    protected abstract BaseResponseMessage CLICK(EventRequestMessage Message);

    protected abstract BaseResponseMessage VIEW(EventRequestMessage Message);

    protected abstract BaseResponseMessage subscribe(EventRequestMessage Message);

    protected abstract BaseResponseMessage unsubscribe(EventRequestMessage Message);

    protected abstract BaseResponseMessage SCAN(EventRequestMessage Message);

    protected abstract BaseResponseMessage LOCATION(EventRequestMessage Message);

    protected abstract BaseResponseMessage scancode_push(EventRequestMessage Message);

    protected abstract BaseResponseMessage scancode_waitmsg(EventRequestMessage Message);

    protected abstract BaseResponseMessage pic_sysphoto(EventRequestMessage Message);

    protected abstract BaseResponseMessage pic_photo_or_album(EventRequestMessage Message);

    protected abstract BaseResponseMessage pic_weixin(EventRequestMessage Message);

    protected abstract BaseResponseMessage location_select(EventRequestMessage Message);
}
