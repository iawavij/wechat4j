/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kuma.wechat4j.constant;

/**
 *
 * @author Kuma
 */
public class WeChatConstant {

    public static final String DEFAULT_CHARSET = "UTF-8";

    //获取access token GET
    public static final String TOKEN = "https://api.weixin.qq.com/cgi-bin/token"; //?grant_type=client_credential&appid=APPID&secret=APPSECRET

    //获取微信服务器IP地址 GET
    public static final String GETCALLBACKIP = "https://api.weixin.qq.com/cgi-bin/getcallbackip"; //?access_token=ACCESS_TOKEN";

    //自定义菜单创建接口 POST
    public static final String MENU_CREATE = "https://api.weixin.qq.com/cgi-bin/menu/create"; //?access_token=ACCESS_TOKEN";

    //自定义菜单查询接口 GET
    public static final String MENU_GET = "https://api.weixin.qq.com/cgi-bin/menu/get"; //?access_token=ACCESS_TOKEN";

    //自定义菜单删除接口 GET
    public static final String MENU_DELETE = "https://api.weixin.qq.com/cgi-bin/menu/delete"; //?access_token=ACCESS_TOKEN";

    //获取自定义菜单配置接口 GET
    public static final String GET_CURRENT_SELFMENU_INFO = "https://api.weixin.qq.com/cgi-bin/get_current_selfmenu_info"; //?access_token=ACCESS_TOKEN

    //获取用户基本信息（包括UnionID机制）GET
    public static final String USER_INFO = "https://api.weixin.qq.com/cgi-bin/user/info"; //?access_token=ACCESS_TOKEN&openid=OPENID&lang=zh_CN

    //发送客服消息 POST
    public static final String SEND_MESSAGE = "https://api.weixin.qq.com/cgi-bin/message/custom/send"; //?access_token=ACCESS_TOKEN"

    //通过code换取网页授权access_token
    public static final String OAUTH2_TOKEN = "https://api.weixin.qq.com/sns/oauth2/access_token"; //?appid=APPID&secret=SECRET&code=CODE&grant_type=authorization_code"
}
