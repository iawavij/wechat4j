/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kuma.wechat4j.common;

import com.kuma.wechat4j.model.ConfigModel;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;
import org.apache.log4j.Logger;

/**
 *
 * @author Kuma
 */
public class Config {

    private static final Logger LOG = Logger.getLogger(Config.class);
    private static final String CONFIG_FILE = "/wechat4j.properties";

    private String token;
    private String encodingAESKey;
    private String appid;
    private String appSecret;

    private static final Config CONFIG = new Config();
    private static ConfigModel cm;

    private Config() {
        Properties p = new Properties();
        cm = new ConfigModel();
        InputStream inStream = this.getClass().getResourceAsStream(CONFIG_FILE);
        if (inStream == null) {
            LOG.error("根目录下找不到wechat4j.properties文件");
            return;
        }
        try {
            p.load(inStream);
            this.token = p.getProperty("wechat.token", "").trim();
            cm.setToken(this.token);
            this.encodingAESKey = p.getProperty("wechat.encodingaeskey", "").trim();
            cm.setEncodingAESKey(this.encodingAESKey);
            this.appid = p.getProperty("wechat.appid", "").trim();
            cm.setAppid(this.appid);
            this.appSecret = p.getProperty("wechat.appsecret", "").trim();
            cm.setAppSecret(this.appSecret);
            inStream.close();
        } catch (IOException ex) {
            LOG.error("load wechat4j.properties error,class根目录下找不到wechat4j.properties文件", ex);
        }
        LOG.debug("load wechat4j.properties success");
    }

    public static Config instance() {
        return CONFIG;
    }

    public static ConfigModel getConfigModel() {
        return cm;
    }

    public String getToken() {
        return token;
    }

    public String getEncodingAESKey() {
        return encodingAESKey;
    }

    public String getAppid() {
        return appid;
    }

    public String getAppSecret() {
        return appSecret;
    }
}
