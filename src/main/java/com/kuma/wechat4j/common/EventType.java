/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kuma.wechat4j.common;

/**
 *
 * @author Kuma
 */
public enum EventType {
    subscribe, //关注事件
    unsubscribe, //取消关注事件
    SCAN, //用户已关注时扫描二维码的事件推送
    LOCATION, //上报地理位置事件

    //创建菜单
    CLICK, //点击菜单拉取消息时的事件推送
    VIEW, //点击菜单跳转链接时的事件推送
    scancode_push, //扫码推事件的事件推送
    scancode_waitmsg, //扫码推事件且弹出“消息接收中”提示框的事件推送
    pic_sysphoto, //弹出系统拍照发图的事件推送
    pic_photo_or_album, //弹出拍照或者相册发图的事件推送
    pic_weixin, //弹出微信相册发图器的事件推送
    location_select; //弹出地理位置选择器的事件推送
}
