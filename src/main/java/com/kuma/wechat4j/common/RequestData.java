/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kuma.wechat4j.common;

import com.alibaba.fastjson.JSONObject;
import com.kuma.wechat4j.constant.WeChatConstant;
import com.kuma.wechat4j.model.CallBackIpModel;
import com.kuma.wechat4j.model.ErrorModel;
import com.kuma.wechat4j.model.MenuGetModel;
import com.kuma.wechat4j.model.SelfMenuInfoModel;
import com.kuma.wechat4j.model.menu.CreateMenuInfo;
import com.kuma.wechat4j.model.userinfo.UserInfo;
import com.kuma.wechat4j.utils.HttpUtils;
import java.util.Map;
import com.kuma.wechat4j.server.CenterServer;
import com.kuma.wechat4j.utils.TypeUtils;
import java.util.HashMap;

/**
 * 与微信服务器进行交互处理数据，需要token的请求
 *
 * @author Kuma
 */
public class RequestData {

    private final CenterServer CS;

    public RequestData() {
        CS = CenterServer.CS;
    }

    public <T> T get(String url, Class<T> clazz) {
        url += "?access_token=" + CS.accessToken();
        JSONObject json = HttpUtils.getJson(url);
        return json.toJavaObject(clazz);
    }

    public <T> T get(String url, Map<String, Object> params, Class<T> clazz) {
        url += "?access_token=" + CS.accessToken();
        JSONObject json = HttpUtils.getJson(url, params);
        return json.toJavaObject(clazz);
    }

    public <T> T post(String url, Map<String, Object> params, Class<T> clazz) {
        url += "?access_token=" + CS.accessToken();
        JSONObject json = HttpUtils.postJson(url, params);
        return json.toJavaObject(clazz);
    }

    /**
     * 获取微信服务器IP地址 GET
     *
     * @return
     */
    public CallBackIpModel getCallBackIp() {
        return get(WeChatConstant.GETCALLBACKIP, CallBackIpModel.class);
    }

    /**
     * 自定义菜单创建接口 POST
     *
     * @param button
     * @return
     */
    public ErrorModel menuCreate(CreateMenuInfo button) {
        return post(WeChatConstant.MENU_CREATE, TypeUtils.transBean2Map(button), ErrorModel.class);
    }

    /**
     * 自定义菜单查询接口 GET
     *
     * @return
     */
    public MenuGetModel menuGet() {
        return get(WeChatConstant.MENU_GET, MenuGetModel.class);
    }

    /**
     * 自定义菜单删除接口 GET
     *
     * @return
     */
    public ErrorModel menuDelete() {
        return get(WeChatConstant.MENU_DELETE, ErrorModel.class);
    }

    /**
     * 获取自定义菜单配置接口 GET
     *
     * @return
     */
    public SelfMenuInfoModel getCurrentSelfmenuInfo() {
        return get(WeChatConstant.GET_CURRENT_SELFMENU_INFO, SelfMenuInfoModel.class);
    }

    /**
     * 获取用户基本信息（包括UnionID机制）GET
     *
     * @param openid
     * @return
     */
    public UserInfo userInfo(String openid) {
        Map<String, Object> params = new HashMap<>();
        params.put("openid", openid);
        return get(WeChatConstant.USER_INFO, params, UserInfo.class);
    }

    /**
     * 发送客服消息 POST
     *
     * @param bean
     * @return
     */
    public ErrorModel sendMessage(Object bean) {
        return post(WeChatConstant.SEND_MESSAGE, TypeUtils.transBean2Map(bean), ErrorModel.class);
    }
}
