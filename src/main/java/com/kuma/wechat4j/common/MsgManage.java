/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kuma.wechat4j.common;

import com.kuma.wechat4j.model.message.req.RequestMessage;
import java.util.HashSet;
import java.util.Set;

/**
 *
 * @author Kuma
 */
public enum MsgManage {
    MM;

    private static final Set<Object> MSG_SET = new HashSet<>();

    private static void add(Object msg) {
        MSG_SET.add(msg);
    }

    private static void remove(Object msg) {
        MSG_SET.remove(msg);
    }

    public static boolean isExist(RequestMessage requestMessage) {
        Object msg;
        ReqType msgType = ReqType.valueOf(requestMessage.getMsgType());
        if (msgType == ReqType.event || requestMessage.getMsgId() == null) {
            msg = requestMessage.getFromUserName() + requestMessage.getCreateTime();
        } else {
            msg = requestMessage.getMsgId();
        }

        boolean rs = MSG_SET.contains(msg);

        if (rs) {
            remove(msg);
        } else {
            add(msg);
        }

        return rs;
    }

    public static void clear(RequestMessage requestMessage) {
        Object msg;
        ReqType msgType = ReqType.valueOf(requestMessage.getMsgType());
        if (msgType == ReqType.event || requestMessage.getMsgId() == null) {
            msg = requestMessage.getFromUserName() + requestMessage.getCreateTime();
        } else {
            msg = requestMessage.getMsgId();
        }
        remove(msg);
    }
}
