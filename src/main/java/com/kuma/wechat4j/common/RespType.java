/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kuma.wechat4j.common;

/**
 * 响应微信请求返回消息的类型
 *
 * @author Kuma
 */
public enum RespType {
    text, //文本消息
    image, //图片消息
    voice, //语音消息
    video, //视频消息
    music, //音乐消息
    news, //图文消息
    
    mpnews, //发送图文消息（点击跳转到图文消息页面） 图文消息条数限制在8条以内，注意，如果图文数超过8，则将会无响应。
    wxcard; //卡券
}
