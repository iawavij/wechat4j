/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kuma.wechat4j.common;

import java.util.Arrays;
import org.apache.commons.codec.digest.DigestUtils;

/**
 *
 * @author Kuma
 */
public class ValidateSignature {

    private final String signature;
    private final String timestamp;
    private final String nonce;
    private final String token;

    public ValidateSignature(String signature, String timestamp, String nonce, String token) {
        this.signature = signature;
        this.timestamp = timestamp;
        this.nonce = nonce;
        this.token = token;
    }

    public boolean check() {
        String sha1 = encode();
        return sha1.equals(this.signature);
    }

    private String encode() {
        String[] arr = {this.token, this.timestamp, this.nonce};
        Arrays.sort(arr); // 字典序排序
        StringBuilder content = new StringBuilder();
        for (String a : arr) {
            content.append(a);
        }
        return DigestUtils.shaHex(content.toString());
    }
}
