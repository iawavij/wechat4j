/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kuma.wechat4j.common;

/**
 *
 * @author Kuma
 */
public enum ReqType {
    text, //文本消息
    image, //图片消息
    voice, //语音消息
    video, //视频消息
    shortvideo, //小视频消息
    location, //地理位置消息
    link, //链接消息
    event;//事件消息
}
