/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kuma.wechat4j.common;

import javax.xml.bind.annotation.adapters.XmlAdapter;

/**
 *
 * @author Kuma
 */
public class CDataAdapter extends XmlAdapter<String, String> {

    @Override
    public String marshal(String str) throws Exception {
        return "<![CDATA[" + str + "]]>";
    }

    @Override
    public String unmarshal(String str) throws Exception {
        return str;
    }
}
