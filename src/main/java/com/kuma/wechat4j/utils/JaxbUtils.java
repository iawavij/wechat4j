/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kuma.wechat4j.utils;

import com.kuma.wechat4j.constant.WeChatConstant;
import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import org.apache.log4j.Logger;

/**
 *
 * @author Kuma
 */
public class JaxbUtils {

    private static final Logger LOG = Logger.getLogger(JaxbUtils.class);

    public static <T> T toBean(String xml, Class<T> clazz) {
        T t = null;
        try {
            JAXBContext context = JAXBContext.newInstance(clazz);
            Unmarshaller unmarshaller = context.createUnmarshaller();
            t = (T) unmarshaller.unmarshal(new StringReader(xml));
        } catch (JAXBException ex) {
            LOG.error("xml2bean error", ex);
        }

        return t;
    }

    public static String toXml(Object bean) {
        String xml = null;
        try {
            JAXBContext context = JAXBContext.newInstance(bean.getClass());
            Marshaller marshaller = context.createMarshaller();
            marshaller.setProperty(Marshaller.JAXB_ENCODING, WeChatConstant.DEFAULT_CHARSET);
            marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
            marshaller.setProperty(Marshaller.JAXB_FRAGMENT, true);// 去掉报文头
            try (StringWriter writer = new StringWriter()) {
                marshaller.marshal(bean, writer);
                xml = writer.toString();
            }
        } catch (JAXBException | IOException ex) {
            LOG.error("bean2xml error", ex);
        }

        if (xml != null) {
            xml = xml.replace("&lt;", "<");
            xml = xml.replace("&gt;", ">");
        }

        LOG.debug("响应微信请求：\n" + xml);
        return xml;
    }
}
