/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kuma.wechat4j.utils;

import com.alibaba.fastjson.JSONObject;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import org.apache.http.Consts;
import org.apache.http.HttpEntity;
import org.apache.http.NameValuePair;
import org.apache.http.client.fluent.Request;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.entity.ContentType;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.apache.log4j.Logger;

/**
 *
 * @author Kuma
 */
public class HttpUtils {

    private static final Logger LOG = Logger.getLogger(HttpUtils.class);
    private static final String METHOD_GET = "GET";
    private static final String METHOD_POST = "POST";

    public static JSONObject getJson(String url) {
        return getJson(url, null);
    }

    public static JSONObject getJson(String url, Map<String, Object> params) {
        return JSONObject.parseObject(get(url, params));
    }

    public static JSONObject postJson(String url, Map<String, Object> params) {
        return JSONObject.parseObject(post(url, params));
    }

    public static String get(String url) {
        return get(url, null);
    }

    public static String get(String url, Map<String, Object> params) {
        return execute(METHOD_GET, url, params);
    }

    public static String post(String url) {
        return post(url, null);
    }

    public static String post(String url, Map<String, Object> params) {
        return execute(METHOD_POST, url, params);
    }

    private static String execute(String method, String url, Map<String, Object> params) {
        LOG.debug(method + " url:" + url);
        LOG.debug("params:" + params);

        String rs = null;
        Request request = null;
        if (method.equalsIgnoreCase(METHOD_GET)) {
            URI uri = null;
            try {
                uri = getURI(url, params);
            } catch (URISyntaxException ex) {
                LOG.error("getURI error\n" + ex.getMessage(), ex);
            }

            request = uri == null ? null : Request.Get(uri);
        }
        if (method.equalsIgnoreCase(METHOD_POST)) {
            request = Request.Post(url).bodyString(JSONObject.toJSONString(params), ContentType.APPLICATION_JSON);
        }

        if (null != request) {
            try {
                HttpEntity entity = request.execute()
                        .returnResponse().getEntity();
                if (entity != null) {
                    rs = EntityUtils.toString(entity, Consts.UTF_8);
                }
            } catch (IOException ex) {
                LOG.error(method + "请求异常，" + ex.getMessage() + "\n" + method + " url:" + url);
            }
        }

        LOG.debug(method + " 请求结果:" + rs);
        return rs;
    }

    private static URI getURI(String url, Map<String, Object> params) throws URISyntaxException {
        URIBuilder uriBuilder = new URIBuilder(URI.create(url));
        if (params != null) {
            uriBuilder.addParameters(getParams(params));
        }
        return uriBuilder.build();
    }

    private static List<NameValuePair> getParams(Map<String, Object> params) {
        List<NameValuePair> nvps = new ArrayList<>();
        if (params != null) {
            for (Map.Entry<String, Object> entrySet : params.entrySet()) {
                String key = entrySet.getKey();
                String value = String.valueOf(params.get(key));
                nvps.add(new BasicNameValuePair(key, value));
            }
        }
        return nvps;
    }
}
