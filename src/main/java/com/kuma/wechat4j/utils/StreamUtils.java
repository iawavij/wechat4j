/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kuma.wechat4j.utils;

import com.kuma.wechat4j.constant.WeChatConstant;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import javax.servlet.http.HttpServletRequest;
import org.apache.log4j.Logger;

/**
 *
 * @author Kuma
 */
public class StreamUtils {

    private static final Logger LOG = Logger.getLogger(StreamUtils.class);

    public static String getXmlString(HttpServletRequest request) {
        String xml = null;
        try (InputStream is = request.getInputStream(); ByteArrayOutputStream baos = new ByteArrayOutputStream()) {
            int i;
            while ((i = is.read()) != -1) {
                baos.write(i);
            }
            xml = baos.toString(WeChatConstant.DEFAULT_CHARSET);
        } catch (IOException ex) {
            LOG.error("从请求中获取xml数据失败！", ex);
        }
        LOG.debug("微信请求内容：\n" + xml);
        return xml;
    }
}
