/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kuma.wechat4j.model;

import com.kuma.wechat4j.model.menu.SelfMenuInfo;

/**
 *
 * @author Kuma
 */
public class SelfMenuInfoModel extends ErrorModel {

    private Integer is_menu_open;
    private SelfMenuInfo selfmenu_info;

    public Integer getIs_menu_open() {
        return is_menu_open;
    }

    public void setIs_menu_open(Integer is_menu_open) {
        this.is_menu_open = is_menu_open;
    }

    public SelfMenuInfo getSelfmenu_info() {
        return selfmenu_info;
    }

    public void setSelfmenu_info(SelfMenuInfo selfmenu_info) {
        this.selfmenu_info = selfmenu_info;
    }

}
