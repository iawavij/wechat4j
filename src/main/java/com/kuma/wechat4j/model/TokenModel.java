/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kuma.wechat4j.model;

/**
 *
 * @author Kuma
 */
public class TokenModel extends ErrorModel {

    private String grant_type = "client_credential";  //获取access_token填写client_credential
    private String appid;           //第三方用户唯一凭证
    private String secret;          //第三方用户唯一凭证密钥，即appsecret

    private String access_token;    //获取到的凭证
    private Long expires_in;        //凭证有效时间，单位：秒

    private long tokenTime;         //token产生时间
    private int redundance = 10 * 1000; //冗余时间，提前10秒就去请求新的token

    public String getGrant_type() {
        return grant_type;
    }

    public void setGrant_type(String grant_type) {
        this.grant_type = grant_type;
    }

    public String getAppid() {
        return appid;
    }

    public void setAppid(String appid) {
        this.appid = appid;
    }

    public String getSecret() {
        return secret;
    }

    public void setSecret(String secret) {
        this.secret = secret;
    }

    public String getAccess_token() {
        return access_token;
    }

    public void setAccess_token(String access_token) {
        this.access_token = access_token;
    }

    public Long getExpires_in() {
        return expires_in;
    }

    public void setExpires_in(Long expires_in) {
        this.expires_in = expires_in;
    }

    public long getTokenTime() {
        return tokenTime;
    }

    public void setTokenTime(long tokenTime) {
        this.tokenTime = tokenTime;
    }

    public int getRedundance() {
        return redundance;
    }

    public void setRedundance(int redundance) {
        this.redundance = redundance;
    }

    @Override
    public String toString() {
        return "AccessTokenModel{" + "grant_type=" + grant_type + ", appid=" + appid + ", secret=" + secret + ", access_token=" + access_token + ", expires_in=" + expires_in + ", tokenTime=" + tokenTime + ", redundance=" + redundance + '}';
    }

}
