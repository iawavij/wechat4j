/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kuma.wechat4j.model;

import java.util.List;
import com.kuma.wechat4j.model.menu.ConditionalMenu;
import com.kuma.wechat4j.model.menu.CreateMenuInfo;

/**
 *
 * @author Kuma
 */
public class MenuGetModel extends ErrorModel {

    private CreateMenuInfo menu;
    private List<ConditionalMenu> conditionalmenu;

    public CreateMenuInfo getMenu() {
        return menu;
    }

    public void setMenu(CreateMenuInfo menu) {
        this.menu = menu;
    }

    public List<ConditionalMenu> getConditionalmenu() {
        return conditionalmenu;
    }

    public void setConditionalmenu(List<ConditionalMenu> conditionalmenu) {
        this.conditionalmenu = conditionalmenu;
    }

}
