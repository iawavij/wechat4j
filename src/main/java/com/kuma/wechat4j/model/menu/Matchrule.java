/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kuma.wechat4j.model.menu;

/**
 *
 * @author Kuma
 */
public class Matchrule {

    private Long group_id;
    private Integer sex;
    private String country;
    private String province;
    private String city;
    private Integer client_platform_type;

    public Long getGroup_id() {
        return group_id;
    }

    public void setGroup_id(Long group_id) {
        this.group_id = group_id;
    }

    public Integer getSex() {
        return sex;
    }

    public void setSex(Integer sex) {
        this.sex = sex;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public Integer getClient_platform_type() {
        return client_platform_type;
    }

    public void setClient_platform_type(Integer client_platform_type) {
        this.client_platform_type = client_platform_type;
    }

}
