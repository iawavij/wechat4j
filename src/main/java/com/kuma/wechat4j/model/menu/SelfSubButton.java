/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kuma.wechat4j.model.menu;

import java.util.List;

/**
 *
 * @author Kuma
 */
public class SelfSubButton {

    private List<SelfButton> list;

    public List<SelfButton> getList() {
        return list;
    }

    public void setList(List<SelfButton> list) {
        this.list = list;
    }

}
