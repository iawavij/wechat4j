/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kuma.wechat4j.model.menu;

import com.kuma.wechat4j.model.menu.SelfButton;
import java.util.List;

/**
 *
 * @author Kuma
 */
public class SelfMenuInfo {

    private List<SelfButton> button;

    public List<SelfButton> getButton() {
        return button;
    }

    public void setButton(List<SelfButton> button) {
        this.button = button;
    }

    @Override
    public String toString() {
        return "SelfMenuInfo{" + "button=" + button + '}';
    }
}
