/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kuma.wechat4j.model.message.req.media;

import javax.xml.bind.annotation.XmlElement;

/**
 *
 * @author Kuma
 */
public class Pic {

    @XmlElement
    private String PicMd5Sum;   //图片的MD5值，开发者若需要，可用于验证接收到图片

    public String getPicMd5Sum() {
        return PicMd5Sum;
    }

    public void setPicMd5Sum(String PicMd5Sum) {
        this.PicMd5Sum = PicMd5Sum;
    }
}
