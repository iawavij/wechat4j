/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kuma.wechat4j.model.message.resp;

import com.kuma.wechat4j.common.RespType;
import com.kuma.wechat4j.model.message.req.BaseRequestMessage;
import com.kuma.wechat4j.model.message.resp.media.Video;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * 回复视频消息
 *
 * @author Kuma
 */
@XmlRootElement(name = "xml")
public class VideoResponseMessage extends BaseResponseMessage {

    private Video Video;

    public VideoResponseMessage() {
    }

    public VideoResponseMessage(BaseRequestMessage requestMessage) {
        super(RespType.video, requestMessage);
    }

    @XmlElement(name = "Video")
    public Video getVideo() {
        return Video;
    }

    public void setVideo(Video Video) {
        this.Video = Video;
    }

}
