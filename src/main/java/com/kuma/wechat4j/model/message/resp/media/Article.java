/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kuma.wechat4j.model.message.resp.media;

import com.kuma.wechat4j.common.CDataAdapter;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

/**
 *
 * @author Kuma
 */
public class Article {

    private String Title; //图文消息标题
    private String Description; //	图文消息描述
    private String PicUrl; //图片链接，支持JPG、PNG格式，较好的效果为大图360*200，小图200*200
    private String Url; //	点击图文消息跳转链接

    @XmlElement(name = "Title")
    @XmlJavaTypeAdapter(CDataAdapter.class)
    public String getTitle() {
        return Title;
    }

    @XmlElement(name = "Description")
    @XmlJavaTypeAdapter(CDataAdapter.class)
    public String getDescription() {
        return Description;
    }

    @XmlElement(name = "PicUrl")
    @XmlJavaTypeAdapter(CDataAdapter.class)
    public String getPicUrl() {
        return PicUrl;
    }

    @XmlElement(name = "Url")
    @XmlJavaTypeAdapter(CDataAdapter.class)
    public String getUrl() {
        return Url;
    }

    public void setTitle(String Title) {
        this.Title = Title;
    }

    public void setDescription(String Description) {
        this.Description = Description;
    }

    public void setPicUrl(String PicUrl) {
        this.PicUrl = PicUrl;
    }

    public void setUrl(String Url) {
        this.Url = Url;
    }

}
