/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kuma.wechat4j.model.message.req.media;

import javax.xml.bind.annotation.XmlElement;

/**
 *
 * @author Kuma
 */
public class SendLocationInfo {

    @XmlElement
    private String Location_X; //X坐标信息
    @XmlElement
    private String Location_Y;//Y坐标信息
    @XmlElement
    private String Scale;	//精度，可理解为精度或者比例尺、越精细的话 scale越高
    @XmlElement
    private String Label;//地理位置的字符串信息
    @XmlElement
    private String Poiname;//朋友圈POI的名字，可能为空

    public String getLocation_X() {
        return Location_X;
    }

    public void setLocation_X(String Location_X) {
        this.Location_X = Location_X;
    }

    public String getLocation_Y() {
        return Location_Y;
    }

    public void setLocation_Y(String Location_Y) {
        this.Location_Y = Location_Y;
    }

    public String getScale() {
        return Scale;
    }

    public void setScale(String Scale) {
        this.Scale = Scale;
    }

    public String getLabel() {
        return Label;
    }

    public void setLabel(String Label) {
        this.Label = Label;
    }

    public String getPoiname() {
        return Poiname;
    }

    public void setPoiname(String Poiname) {
        this.Poiname = Poiname;
    }

}
