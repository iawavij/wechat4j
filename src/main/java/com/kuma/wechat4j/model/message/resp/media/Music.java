/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kuma.wechat4j.model.message.resp.media;

import com.kuma.wechat4j.common.CDataAdapter;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

/**
 *
 * @author Kuma
 */
public class Music {

    private String Title; //音乐标题
    private String Description; //	音乐描述
    private String MusicURL; //	音乐链接
    private String HQMusicUrl; //	高质量音乐链接，WIFI环境优先使用该链接播放音乐
    private String ThumbMediaId; //	缩略图的媒体id，通过素材管理中的接口上传多媒体文件，得到的id

    @XmlElement(name = "Title")
    @XmlJavaTypeAdapter(CDataAdapter.class)
    public String getTitle() {
        return Title;
    }

    @XmlElement(name = "Description")
    @XmlJavaTypeAdapter(CDataAdapter.class)
    public String getDescription() {
        return Description;
    }

    @XmlElement(name = "MusicURL")
    @XmlJavaTypeAdapter(CDataAdapter.class)
    public String getMusicURL() {
        return MusicURL;
    }

    @XmlElement(name = "HQMusicUrl")
    @XmlJavaTypeAdapter(CDataAdapter.class)
    public String getHQMusicUrl() {
        return HQMusicUrl;
    }

    @XmlElement(name = "ThumbMediaId")
    @XmlJavaTypeAdapter(CDataAdapter.class)
    public String getThumbMediaId() {
        return ThumbMediaId;
    }

    public void setTitle(String Title) {
        this.Title = Title;
    }

    public void setDescription(String Description) {
        this.Description = Description;
    }

    public void setMusicURL(String MusicURL) {
        this.MusicURL = MusicURL;
    }

    public void setHQMusicUrl(String HQMusicUrl) {
        this.HQMusicUrl = HQMusicUrl;
    }

    public void setThumbMediaId(String ThumbMediaId) {
        this.ThumbMediaId = ThumbMediaId;
    }
}
