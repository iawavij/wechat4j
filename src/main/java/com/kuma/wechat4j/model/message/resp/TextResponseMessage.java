/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kuma.wechat4j.model.message.resp;

import com.kuma.wechat4j.common.CDataAdapter;
import com.kuma.wechat4j.common.RespType;
import com.kuma.wechat4j.model.message.req.BaseRequestMessage;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

/**
 * 回复文本消息
 *
 * @author Kuma
 */
@XmlRootElement(name = "xml")
public class TextResponseMessage extends BaseResponseMessage {

    private String Content; //回复的消息内容（换行：在content中能够换行，微信客户端就支持换行显示）

    public TextResponseMessage() {
    }

    public TextResponseMessage(BaseRequestMessage requestMessage) {
        super(RespType.text, requestMessage);
    }

    @XmlElement(name = "Content")
    @XmlJavaTypeAdapter(CDataAdapter.class)
    public String getContent() {
        return Content;
    }

    public void setContent(String Content) {
        this.Content = Content;
    }
}
