/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kuma.wechat4j.model.message.resp.media;

import com.kuma.wechat4j.common.CDataAdapter;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

/**
 *
 * @author Kuma
 */
public class Video {

    private String MediaId;//通过素材管理中的接口上传多媒体文件，得到的id
    private String Title;//视频消息的标题
    private String Description;//视频消息的描述

    @XmlElement(name = "MediaId")
    @XmlJavaTypeAdapter(CDataAdapter.class)
    public String getMediaId() {
        return MediaId;
    }

    public void setMediaId(String MediaId) {
        this.MediaId = MediaId;
    }

    @XmlElement(name = "Title")
    @XmlJavaTypeAdapter(CDataAdapter.class)
    public String getTitle() {
        return Title;
    }

    public void setTitle(String Title) {
        this.Title = Title;
    }

    @XmlElement(name = "Description")
    @XmlJavaTypeAdapter(CDataAdapter.class)
    public String getDescription() {
        return Description;
    }

    public void setDescription(String Description) {
        this.Description = Description;
    }
}
