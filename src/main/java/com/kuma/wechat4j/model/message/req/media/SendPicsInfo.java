/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kuma.wechat4j.model.message.req.media;

import java.util.List;
import javax.xml.bind.annotation.XmlElement;

/**
 *
 * @author Kuma
 */
public class SendPicsInfo {

    @XmlElement
    private Integer Count;   //发送的图片数量
    @XmlElement
    private List<Pic> PicList; //图片列表

    public Integer getCount() {
        return Count;
    }

    public void setCount(Integer Count) {
        this.Count = Count;
    }

    public List<Pic> getPicList() {
        return PicList;
    }

    public void setPicList(List<Pic> PicList) {
        this.PicList = PicList;
    }

}
