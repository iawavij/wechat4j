/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kuma.wechat4j.model.message.resp.send;

/**
 *
 * @author Kuma
 */
public class Content {

    private String content;         //文本消息内容
    private String media_id;        //发送的图片/语音/视频/图文消息（点击跳转到图文消息页）的媒体ID
    private String thumb_media_id;  //缩略图的媒体ID
    private String title;           //图文消息/视频消息/音乐消息的标题
    private String description;     //图文消息/视频消息/音乐消息的描述
    private String musicurl;        //音乐链接
    private String hqmusicurl;      //高品质音乐链接，wifi环境优先使用该链接播放音乐
    private String url;             //图文消息被点击后跳转的链接
    private String picurl;          //图文消息的图片链接，支持JPG、PNG格式，较好的效果为大图640*320，小图80*80
    private String card_id;         //卡卷ID

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getMedia_id() {
        return media_id;
    }

    public void setMedia_id(String media_id) {
        this.media_id = media_id;
    }

    public String getThumb_media_id() {
        return thumb_media_id;
    }

    public void setThumb_media_id(String thumb_media_id) {
        this.thumb_media_id = thumb_media_id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getMusicurl() {
        return musicurl;
    }

    public void setMusicurl(String musicurl) {
        this.musicurl = musicurl;
    }

    public String getHqmusicurl() {
        return hqmusicurl;
    }

    public void setHqmusicurl(String hqmusicurl) {
        this.hqmusicurl = hqmusicurl;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getPicurl() {
        return picurl;
    }

    public void setPicurl(String picurl) {
        this.picurl = picurl;
    }

    public String getCard_id() {
        return card_id;
    }

    public void setCard_id(String card_id) {
        this.card_id = card_id;
    }
}
