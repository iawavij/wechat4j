/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kuma.wechat4j.model.message.resp;

import com.kuma.wechat4j.common.RespType;
import com.kuma.wechat4j.model.message.req.BaseRequestMessage;
import com.kuma.wechat4j.model.message.resp.media.Article;
import java.util.List;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Kuma
 */
@XmlRootElement(name = "xml")
public class NewsResponseMessage extends BaseResponseMessage {

    private Integer ArticleCount; //	图文消息个数，限制为10条以内
    private List<Article> Articles; //	多条图文消息信息，默认第一个item为大图,注意，如果图文数超过10，则将会无响应

    public NewsResponseMessage() {
    }

    public NewsResponseMessage(BaseRequestMessage requestMessage) {
        super(RespType.news, requestMessage);
    }

    @XmlElement(name = "ArticleCount")
    public Integer getArticleCount() {
        return ArticleCount;
    }

    @XmlElementWrapper(name="Articles")
    @XmlElement(name="item")
    public List<Article> getArticles() {
        return Articles;
    }

    public void setArticleCount(Integer ArticleCount) {
        this.ArticleCount = ArticleCount;
    }

    public void setArticles(List<Article> Articles) {
        this.Articles = Articles;
    }
}
