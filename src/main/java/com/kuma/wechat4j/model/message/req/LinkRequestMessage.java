/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kuma.wechat4j.model.message.req;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * 链接消息
 *
 * @author Kuma
 */
@XmlRootElement(name = "xml")
public class LinkRequestMessage extends BaseRequestMessage {

    @XmlElement
    private String Title;	//消息标题
    @XmlElement
    private String Description;	//消息描述
    @XmlElement
    private String Url;    //消息链接

    public String getTitle() {
        return Title;
    }

    public void setTitle(String Title) {
        this.Title = Title;
    }

    public String getDescription() {
        return Description;
    }

    public void setDescription(String Description) {
        this.Description = Description;
    }

    public String getUrl() {
        return Url;
    }

    public void setUrl(String Url) {
        this.Url = Url;
    }

}
