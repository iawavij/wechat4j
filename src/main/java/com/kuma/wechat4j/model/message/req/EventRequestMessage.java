/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kuma.wechat4j.model.message.req;

import com.kuma.wechat4j.model.message.req.media.SendPicsInfo;
import com.kuma.wechat4j.model.message.req.media.SendLocationInfo;
import com.kuma.wechat4j.model.message.req.media.ScanCodeInfo;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * 事件消息 推送
 *
 * @author Kuma
 */
@XmlRootElement(name = "xml")
public class EventRequestMessage extends BaseRequestMessage {

    @XmlElement
    private String Event;	//事件类型，subscribe(订阅)、unsubscribe(取消订阅)
    @XmlElement
    private String EventKey;	//事件KEY值，与自定义菜单接口中KEY值对应
    @XmlElement
    private String MenuID;      //指菜单ID，如果是个性化菜单，则可以通过这个字段，知道是哪个规则的菜单被点击了。
    @XmlElement
    private String Ticket; //二维码的ticket，可用来换取二维码图片
    @XmlElement
    private Double Latitude;//地理位置纬度
    @XmlElement
    private Double Longitude;//地理位置经度
    @XmlElement
    private Double Precision;//地理位置精度
    @XmlElement
    private ScanCodeInfo ScanCodeInfo; //扫描信息
    @XmlElement
    private SendPicsInfo SendPicsInfo; //发送的图片信息
    @XmlElement
    private SendLocationInfo SendLocationInfo; //发送的位置信息

    public String getEvent() {
        return Event;
    }

    public void setEvent(String Event) {
        this.Event = Event;
    }

    public String getEventKey() {
        return EventKey;
    }

    public void setEventKey(String EventKey) {
        this.EventKey = EventKey;
    }

    public String getMenuID() {
        return MenuID;
    }

    public void setMenuID(String MenuID) {
        this.MenuID = MenuID;
    }

    public String getTicket() {
        return Ticket;
    }

    public void setTicket(String Ticket) {
        this.Ticket = Ticket;
    }

    public Double getLatitude() {
        return Latitude;
    }

    public void setLatitude(Double Latitude) {
        this.Latitude = Latitude;
    }

    public Double getLongitude() {
        return Longitude;
    }

    public void setLongitude(Double Longitude) {
        this.Longitude = Longitude;
    }

    public Double getPrecision() {
        return Precision;
    }

    public void setPrecision(Double Precision) {
        this.Precision = Precision;
    }

    public ScanCodeInfo getScanCodeInfo() {
        return ScanCodeInfo;
    }

    public void setScanCodeInfo(ScanCodeInfo ScanCodeInfo) {
        this.ScanCodeInfo = ScanCodeInfo;
    }

    public SendPicsInfo getSendPicsInfo() {
        return SendPicsInfo;
    }

    public void setSendPicsInfo(SendPicsInfo SendPicsInfo) {
        this.SendPicsInfo = SendPicsInfo;
    }

    public SendLocationInfo getSendLocationInfo() {
        return SendLocationInfo;
    }

    public void setSendLocationInfo(SendLocationInfo SendLocationInfo) {
        this.SendLocationInfo = SendLocationInfo;
    }
}
