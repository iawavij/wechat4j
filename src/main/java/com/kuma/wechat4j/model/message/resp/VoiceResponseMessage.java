/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kuma.wechat4j.model.message.resp;

import com.kuma.wechat4j.common.RespType;
import com.kuma.wechat4j.model.message.req.BaseRequestMessage;
import com.kuma.wechat4j.model.message.resp.media.Voice;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * 回复语音消息
 *
 * @author Kuma
 */
@XmlRootElement(name = "xml")
public class VoiceResponseMessage extends BaseResponseMessage {

    private Voice Voice;

    public VoiceResponseMessage() {
    }

    public VoiceResponseMessage(BaseRequestMessage requestMessage) {
        super(RespType.voice, requestMessage);
    }

    @XmlElement(name = "Voice")
    public Voice getVoice() {
        return Voice;
    }

    public void setVoice(Voice Voice) {
        this.Voice = Voice;
    }

}
