/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kuma.wechat4j.model.message.resp;

import com.kuma.wechat4j.common.RespType;
import com.kuma.wechat4j.model.message.req.BaseRequestMessage;
import com.kuma.wechat4j.model.message.resp.media.Music;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * 回复音乐消息
 *
 * @author Kuma
 */
@XmlRootElement(name = "xml")
public class MusicResponseMessage extends BaseResponseMessage {

    private Music Music;

    public MusicResponseMessage() {
    }

    public MusicResponseMessage(BaseRequestMessage requestMessage) {
        super(RespType.music, requestMessage);
    }

    @XmlElement(name = "Music")
    public Music getMusic() {
        return Music;
    }

    public void setMusic(Music Music) {
        this.Music = Music;
    }

}
