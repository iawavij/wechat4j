/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kuma.wechat4j.model.message.resp.send;

import com.kuma.wechat4j.model.message.resp.BaseResponseMessage;

/**
 *
 * @author Kuma
 */
public class BaseMessage {

    private String touser;  //普通用户openid
    private String msgtype; //消息类型，文本为text，图片为image，语音为voice，视频消息为video，音乐消息为music，图文消息（点击跳转到外链）为news，图文消息（点击跳转到图文消息页面）为mpnews，卡券为wxcard

    private Content text; //文本消息
    private Content image;
    private Content voice;
    private Content video;
    private Content music;
    private News news;
    private Content mpnews;
    private Content wxcard;

    private Custom customservice; //客服信息

    public BaseMessage() {
    }

    public BaseMessage(BaseResponseMessage responseMessage) {
        this.touser = responseMessage.getToUserName();
        this.msgtype = responseMessage.getMsgType();
    }

    public String getTouser() {
        return touser;
    }

    public void setTouser(String touser) {
        this.touser = touser;
    }

    public String getMsgtype() {
        return msgtype;
    }

    public void setMsgtype(String msgtype) {
        this.msgtype = msgtype;
    }

    public Content getText() {
        return text;
    }

    public void setText(Content text) {
        this.text = text;
    }

    public Content getImage() {
        return image;
    }

    public void setImage(Content image) {
        this.image = image;
    }

    public Content getVoice() {
        return voice;
    }

    public void setVoice(Content voice) {
        this.voice = voice;
    }

    public Content getVideo() {
        return video;
    }

    public void setVideo(Content video) {
        this.video = video;
    }

    public Content getMusic() {
        return music;
    }

    public void setMusic(Content music) {
        this.music = music;
    }

    public News getNews() {
        return news;
    }

    public void setNews(News news) {
        this.news = news;
    }

    public Content getMpnews() {
        return mpnews;
    }

    public void setMpnews(Content mpnews) {
        this.mpnews = mpnews;
    }

    public Content getWxcard() {
        return wxcard;
    }

    public void setWxcard(Content wxcard) {
        this.wxcard = wxcard;
    }

    public Custom getCustomservice() {
        return customservice;
    }

    public void setCustomservice(Custom customservice) {
        this.customservice = customservice;
    }
}
