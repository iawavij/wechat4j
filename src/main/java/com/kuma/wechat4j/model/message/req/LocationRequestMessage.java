/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kuma.wechat4j.model.message.req;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * 地理位置消息
 *
 * @author Kuma
 */
@XmlRootElement(name = "xml")
public class LocationRequestMessage extends BaseRequestMessage {

    @XmlElement
    private Double Location_X;	//地理位置维度
    @XmlElement
    private Double Location_Y;	//地理位置经度
    @XmlElement
    private Double Scale;	//地图缩放大小
    @XmlElement
    private String Label;	//地理位置信息

    public Double getLocation_X() {
        return Location_X;
    }

    public void setLocation_X(Double Location_X) {
        this.Location_X = Location_X;
    }

    public Double getLocation_Y() {
        return Location_Y;
    }

    public void setLocation_Y(Double Location_Y) {
        this.Location_Y = Location_Y;
    }

    public Double getScale() {
        return Scale;
    }

    public void setScale(Double Scale) {
        this.Scale = Scale;
    }

    public String getLabel() {
        return Label;
    }

    public void setLabel(String Label) {
        this.Label = Label;
    }

}
