/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kuma.wechat4j.model.message.resp.media;

import com.kuma.wechat4j.common.CDataAdapter;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

/**
 *
 * @author Kuma
 */
public class Image {

    private String MediaId; // 通过素材管理中的接口上传多媒体文件，得到的id。

    @XmlElement(name = "MediaId")
    @XmlJavaTypeAdapter(CDataAdapter.class)
    public String getMediaId() {
        return MediaId;
    }

    public void setMediaId(String MediaId) {
        this.MediaId = MediaId;
    }

}
