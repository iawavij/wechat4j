/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kuma.wechat4j.model.message.resp;

import com.kuma.wechat4j.common.CDataAdapter;
import com.kuma.wechat4j.common.RespType;
import com.kuma.wechat4j.model.message.req.BaseRequestMessage;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

/**
 * 回复消息基类
 *
 * @author Kuma
 */
@XmlRootElement(name = "xml")
public class BaseResponseMessage {

    private String ToUserName; // 接收方帐号（收到的OpenID）
    private String FromUserName; // 开发者微信号
    private Long CreateTime; // 消息创建时间 （整型）
    private String MsgType; // 消息类型（text/image/voice/video/music/news）

    public BaseResponseMessage() {
    }

    public BaseResponseMessage(RespType msgType, BaseRequestMessage requestMessage) {
        this.MsgType = msgType.name();
        this.ToUserName = requestMessage.getFromUserName();
        this.FromUserName = requestMessage.getToUserName();
        this.CreateTime = requestMessage.getCreateTime();
    }

    @XmlElement(name = "ToUserName")
    @XmlJavaTypeAdapter(CDataAdapter.class)
    public String getToUserName() {
        return ToUserName;
    }

    @XmlElement(name = "FromUserName")
    @XmlJavaTypeAdapter(CDataAdapter.class)
    public String getFromUserName() {
        return FromUserName;
    }

    @XmlElement(name = "CreateTime")
    public Long getCreateTime() {
        return CreateTime;
    }

    @XmlElement(name = "MsgType")
    @XmlJavaTypeAdapter(CDataAdapter.class)
    public String getMsgType() {
        return MsgType;
    }

    public void setToUserName(String ToUserName) {
        this.ToUserName = ToUserName;
    }

    public void setFromUserName(String FromUserName) {
        this.FromUserName = FromUserName;
    }

    public void setCreateTime(Long CreateTime) {
        this.CreateTime = CreateTime;
    }

    public void setMsgType(String MsgType) {
        this.MsgType = MsgType;
    }
}
