/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kuma.wechat4j.model.message.req;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * 语音消息
 *
 * @author Kuma
 */
@XmlRootElement(name = "xml")
public class VoiceRequestMessage extends BaseRequestMessage {

    @XmlElement
    private String MediaId; //媒体id，可以调用多媒体文件下载接口拉取数据。
    @XmlElement
    private String Format; //语音格式，如amr，speex等
    @XmlElement
    private String Recognition; //语音识别结果，UTF8编码

    public String getMediaId() {
        return MediaId;
    }

    public void setMediaId(String MediaId) {
        this.MediaId = MediaId;
    }

    public String getFormat() {
        return Format;
    }

    public void setFormat(String Format) {
        this.Format = Format;
    }

    public String getRecognition() {
        return Recognition;
    }

    public void setRecognition(String Recognition) {
        this.Recognition = Recognition;
    }
}
