/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kuma.wechat4j.model.message.req.media;

import javax.xml.bind.annotation.XmlElement;

/**
 *
 * @author Kuma
 */
public class ScanCodeInfo {

    @XmlElement
    private String ScanType;    //扫描类型，一般是qrcode
    @XmlElement
    private String ScanResult;  //扫描结果，即二维码对应的字符串信息

    public String getScanType() {
        return ScanType;
    }

    public void setScanType(String ScanType) {
        this.ScanType = ScanType;
    }

    public String getScanResult() {
        return ScanResult;
    }

    public void setScanResult(String ScanResult) {
        this.ScanResult = ScanResult;
    }

}
