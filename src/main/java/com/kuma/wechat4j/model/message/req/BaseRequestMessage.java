/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kuma.wechat4j.model.message.req;

import javax.xml.bind.annotation.XmlElement;

/**
 * 消息基类（普通用户 -> 公众帐号） 微信请求过来的消息
 *
 * @author Kuma
 */
public class BaseRequestMessage {

    @XmlElement
    private String ToUserName; // 开发者微信号
    @XmlElement
    private String FromUserName; // 发送方帐号（一个OpenID）
    @XmlElement
    private Long CreateTime; // 消息创建时间 （整型）
    @XmlElement
    private String MsgType; // 消息类型（text/image/location/link）
    @XmlElement
    private Long MsgId; // 消息id，64位整型

    public String getToUserName() {
        return ToUserName;
    }

    public void setToUserName(String ToUserName) {
        this.ToUserName = ToUserName;
    }

    public String getFromUserName() {
        return FromUserName;
    }

    public void setFromUserName(String FromUserName) {
        this.FromUserName = FromUserName;
    }

    public Long getCreateTime() {
        return CreateTime;
    }

    public void setCreateTime(Long CreateTime) {
        this.CreateTime = CreateTime;
    }

    public String getMsgType() {
        return MsgType;
    }

    public void setMsgType(String MsgType) {
        this.MsgType = MsgType;
    }

    public Long getMsgId() {
        return MsgId;
    }

    public void setMsgId(Long MsgId) {
        this.MsgId = MsgId;
    }
}
