/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kuma.wechat4j.model.message.resp;

import com.kuma.wechat4j.common.RespType;
import com.kuma.wechat4j.model.message.req.BaseRequestMessage;
import com.kuma.wechat4j.model.message.resp.media.Image;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * 回复图片消息
 *
 * @author Kuma
 */
@XmlRootElement(name = "xml")
public class ImageResponseMessage extends BaseResponseMessage {

    private Image Image;

    public ImageResponseMessage() {
    }

    public ImageResponseMessage(BaseRequestMessage requestMessage) {
        super(RespType.image, requestMessage);
    }

    @XmlElement(name = "Image")
    public Image getImage() {
        return Image;
    }

    public void setImage(Image Image) {
        this.Image = Image;
    }
}
