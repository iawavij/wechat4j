/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kuma.wechat4j;

import com.kuma.wechat4j.common.RequestData;
import com.kuma.wechat4j.common.RespType;
import com.kuma.wechat4j.model.ErrorModel;
import com.kuma.wechat4j.model.message.resp.BaseResponseMessage;
import com.kuma.wechat4j.model.message.resp.ImageResponseMessage;
import com.kuma.wechat4j.model.message.resp.MusicResponseMessage;
import com.kuma.wechat4j.model.message.resp.NewsResponseMessage;
import com.kuma.wechat4j.model.message.resp.TextResponseMessage;
import com.kuma.wechat4j.model.message.resp.VideoResponseMessage;
import com.kuma.wechat4j.model.message.resp.VoiceResponseMessage;
import com.kuma.wechat4j.model.message.resp.media.Article;
import com.kuma.wechat4j.model.message.resp.media.Music;
import com.kuma.wechat4j.model.message.resp.media.Video;
import com.kuma.wechat4j.model.message.resp.send.BaseMessage;
import com.kuma.wechat4j.model.message.resp.send.Content;
import com.kuma.wechat4j.model.message.resp.send.News;
import java.util.ArrayList;
import java.util.List;
import org.apache.log4j.Logger;

/**
 *
 * @author Kuma
 */
public class MessageHandle {

    private static final Logger LOG = Logger.getLogger(MessageHandle.class);
    private static final RequestData QUEST = new RequestData();

    private BaseResponseMessage responseMessage;
    private BaseMessage message;

    private MessageHandle() {
    }

    public MessageHandle(BaseResponseMessage responseMessage) {
        this.responseMessage = responseMessage;
    }

    public void send() {
        RespType type = RespType.valueOf(responseMessage.getMsgType());
        message = new BaseMessage(responseMessage);
        Content content = new Content();

        switch (type) {
            case text:
                TextResponseMessage textResponse = (TextResponseMessage) this.responseMessage;
                message.setText(content);
                content.setContent(textResponse.getContent());
                break;
            case image:
                ImageResponseMessage imageResponse = (ImageResponseMessage) this.responseMessage;
                message.setImage(content);
                content.setMedia_id(imageResponse.getImage().getMediaId());
                break;
            case voice:
                VoiceResponseMessage voiceResponse = (VoiceResponseMessage) this.responseMessage;
                message.setVoice(content);
                content.setMedia_id(voiceResponse.getVoice().getMediaId());
                break;
            case video:
                VideoResponseMessage videoResponse = (VideoResponseMessage) this.responseMessage;
                message.setVideo(content);
                Video video = videoResponse.getVideo();
                content.setMedia_id(video.getMediaId());
                content.setTitle(video.getTitle());
                content.setDescription(video.getDescription());
                //content.setThumb_media_id(thumb_media_id);
                break;
            case music:
                MusicResponseMessage musicResponse = (MusicResponseMessage) this.responseMessage;
                message.setMusic(content);
                Music music = musicResponse.getMusic();

                content.setTitle(music.getTitle());
                content.setDescription(music.getDescription());
                content.setMusicurl(music.getMusicURL());
                content.setHqmusicurl(music.getHQMusicUrl());
                content.setThumb_media_id(music.getThumbMediaId());
                break;
            case news:
                NewsResponseMessage newsResponse = (NewsResponseMessage) this.responseMessage;
                List<Article> list = newsResponse.getArticles();

                News news = new News();
                List<Content> articles = new ArrayList<>();
                news.setArticles(articles);
                message.setNews(news);

                for (Article article : list) {
                    Content ct = new Content();
                    articles.add(ct);
                    ct.setTitle(article.getTitle());
                    ct.setDescription(article.getDescription());
                    ct.setUrl(article.getUrl());
                    ct.setPicurl(article.getPicUrl());
                }
                break;
            case mpnews:
                break;
            case wxcard:
                break;
            default:
                LOG.warn("未知的消息类型：" + type);
                break;
        }

        start();
    }

    private void start() {
        ErrorModel rs = QUEST.sendMessage(message);
        if (rs.getErrcode() != 0) {
            LOG.warn("发送客服消息失败，消息类型：" + message.getMsgtype() + " |错误信息：" + rs);
        }
    }
}
